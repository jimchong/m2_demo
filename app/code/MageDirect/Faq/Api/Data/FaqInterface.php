<?php

namespace MageDirect\Faq\Api\Data;

interface FaqInterface
{
    /**
     * Constants for keys of data array
     */
    const FAQ_ID = 'faq_id';
    const TITLE = 'title';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const IS_ACTIVE = 'is_active';

    public function getId();

    public function setId($id);

    public function getTitle();

    public function setTitle($title);

    public function getContent();

    public function setContent($content);

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);

    public function isActive();

    public function setIsActive($isActive);

}
