<?php
namespace Coolryan\PluginExample\Plugin;

class FilterCategoryName
{
    public function beforeSetCategoryCollection(
        \Magento\Catalog\Model\Product $product,
        \Magento\Framework\Data\Collection $categoryCollection
    ) {
        $categoryCollection->addFieldToFilter('name', ['eq' => 'books']);
    }
}
