<?php

namespace Pulsestorm\ToDoCrud\Model;

use Magento\Framework\Api\SearchResults;
use Pulsestorm\ToDoCrud\Api\Data\TodoItemSearchResultInterface;

class TodoItemSearchResult extends SearchResults implements TodoItemSearchResultInterface
{

}