<?php
namespace JimChong\TodoAdminBackend\Controller\Adminhtml\Index;

class Index extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'JimChong_TodoAdminBackend::jimchong_todo_grid_menu';

    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->resultPageFactory->create();
    }

    public function _isAllowed()
    {
        return $this->_authorization->isAllowed('JimChong_TodoAdminBackend::menu_1');
    }

}
