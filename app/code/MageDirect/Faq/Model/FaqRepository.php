<?php

namespace MageDirect\Faq\Model;

use MageDirect\Faq\Api\Data\FaqInterface;
use MageDirect\Faq\Api\Data\FaqSearchResultsInterfaceFactory;
use MageDirect\Faq\Api\FaqRepositoryInterface;
use MageDirect\Faq\Model\ResourceModel\Faq as ResourceFaq;
use MageDirect\Faq\Model\ResourceModel\Faq\CollectionFactory as FaqCollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class FaqRepository implements FaqRepositoryInterface
{
    protected $resource;

    protected $faqFactory;

    protected $searchResultsFactory;

    protected $collectionProcessor;

    protected $instances = [];

    /**
     * FaqRepository constructor.
     */
    public function __construct(
        ResourceFaq $resource,
        FaqFactory $faqFactory,
        FaqCollectionFactory $faqCollectionFactory,
        FaqSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    )
    {
        $this->resource = $resource;
        $this->faqFactory = $faqFactory;
        $this->faqCollectionFactory = $faqCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param FaqInterface $faq
     */
    public function save(FaqInterface $faq)
    {
        try {
            $this->resource->save($faq);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        unset($this->instance[$faq->getId()]);
        return $faq;
    }

    public function getById($faqId)
    {
        if (!isset($this->instances[$faqId])) {
            $faq = $this->faqFactory->create();
            $this->resource->load($faq, $faqId);
            if (!$faq->getId()) {
                $this->instances[$faqId] = $faq;
            }
            $this->instances[$faqId] = $faq;
        }
        return $this->instances[$faqId];
    }

    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->faqCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());
        $searchResults->setTotlaCount($collection->getSize());
        return $searchResults;
    }

    public function delete(FaqInterface $faq)
    {
        try {
            $faqId = $faq->getId();
            $this->resource->delete($faq);
        } catch(\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        unset($this->instances[$faqId]);
        return true;
    }

    public function deleteById($faqId)
    {
        return $this->delete($this->getById($faqId));
    }
}
