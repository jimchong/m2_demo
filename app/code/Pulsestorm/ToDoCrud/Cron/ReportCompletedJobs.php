<?php
namespace Pulsestorm\ToDoCrud\Cron;

use Pulsestorm\ToDoCrud\Api\TodoItemRepositoryInterface;
use Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface;
use Pulsestorm\ToDoCrud\Model\TodoItemFactory;
use Pulsestorm\ToDoCrud\Model\ResourceModel\TodoItem as ObjectResourceModel;
use Pulsestorm\ToDoCrud\Model\ResourceModel\TodoItem\CollectionFactory;
use Pulsestorm\ToDoCrud\Logger\Logger;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;


class ReportCompletedJobs
{
    protected $_logger;
    protected $_date;
    protected $collectionFactory;
    protected $timezone;


    public function __construct(
        DateTime $date,
        Logger $logger,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezone

    ) {
        $this->_date                = $date;
        $this->_logger              = $logger;
        $this->collectionFactory    = $collectionFactory;
        $this->timezone             = $timezone;
//        $this->timezone->date($date)->format($this->_date);
    }


    public function execute()
    {
        date_default_timezone_set('EST');
        $today = $this->_date->gmtDate();

        $collection = $this->collectionFactory->create();


        // foreach ($collection as $value) {
        //     // $value->delete();
        //     // if (isset($value->date_completed)) {

        //     //     // $collection->forget($value);
        //     // }
        // }

//        $todoCreated = $collection->addFieldToFilter('date_completed', ['lteq' => $this->_date->date('Y-m-d')]);


        $todo = json_encode($collection->addFieldToFilter('date_completed', ['lteq' => $this->_date->date('Y-m-d')])
            ->addFieldToFilter('date_completed', ['gteq' => $this->_date->date('Y-m-d')])->getData());

        $this->_logger->info("timezone is : " . $this->_date->date('Y-m-d'));
        $this->_logger->info("cronjob executed" . "at " . $this->_date->gmtDate());



        file_put_contents("todo_completed.json", $todo);

    }

}
