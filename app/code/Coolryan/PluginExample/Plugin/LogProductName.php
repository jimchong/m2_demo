<?php
namespace Coolryan\PluginExample\Plugin;

class LogProductName
{
    protected $logger;

    public function __construct(
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->logger = $logger;
    }

    public function afterGetName(
        \Magento\Catalog\Model\Product $product
    ) {
        $this->logger->debug($product->getName());
    }
}
