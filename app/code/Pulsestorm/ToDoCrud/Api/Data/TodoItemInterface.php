<?php
namespace Pulsestorm\ToDoCrud\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface TodoItemInterface extends ExtensibleDataInterface
{

    /**
     * @param $text
     * @return mixed
     */
    public function setItemText($text);

    /**
     * @return mixed
     */
    public function getItemText();

}