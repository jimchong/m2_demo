<?php
namespace Pulsestorm\ToDoCrud\Model;

use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface;

use Pulsestorm\ToDoCrud\Api\TodoItemRepositoryInterface;
use Pulsestorm\ToDoCrud\Logger\Logger;
use Pulsestorm\ToDoCrud\Model\ResourceModel\TodoItem as ObjectResourceModel;
use Pulsestorm\ToDoCrud\Model\ResourceModel\TodoItem\CollectionFactory;

class TodoItemRepository implements TodoItemRepositoryInterface
{
    protected $objectFactory;
    protected $objectResourceModel;
    protected $collectionFactory;
    protected $searchResultsFactory;
    protected $_logger;
    protected $_date;

    public function __construct(
        TodoItemFactory $objectFactory,
        ObjectResourceModel $objectResourceModel,
        CollectionFactory $collectionFactory,
        DateTime $date,
        Logger $logger
    ) {
        $this->objectFactory        = $objectFactory;
        $this->objectResourceModel  = $objectResourceModel;
        $this->collectionFactory    = $collectionFactory;
        $this->_date                = $date;
        $this->_logger              = $logger;
    }
//
//    public function put($id, TodoItemInterface $TodoItem)
//    {
//        $todo = $this->getById($id);
//        if (!$todo->getId()) {
//            throw new NoSuchEntityException(__('Object with id '. $id .' does not exist.', $id));
//        }
//        $todo->setData($TodoItem);
    ////        $this->_logger->info('I did put call');
//
//        $todo->save();
//        return $todo->getData();
//    }

    /**
    * data should be data
    *
    **/

    public function save($data)
    {
        // logger
        // $this->_logger->info('Inside save method');

        $todo = $this->objectFactory->create();

        // case update
        if (array_key_exists('id', $data)) {
            $id = $data['id'];
            $this->objectResourceModel->load($todo, $id);
            if (!$todo->getId()) {
                throw new NoSuchEntityException(__('Object with id ' . $id . ' does not exist.', $id));
            }

            if (array_key_exists('item_text', $data)) {
                $todo->setData('item_text', $data['item_text']);
            }

            if (array_key_exists('date_completed', $data)) {
                $todo->setData('date_completed', $data['date_completed']);
            }

            $todo->setData('update_time', $this->_date->gmtDate());
            $todo->save();
        } else {

        // case create
            try {
                if (array_key_exists('item_text', $data)) {
                    $todo->setData('item_text', $data['item_text']);
                }
                $todo->setData('creation_time', $this->_date->gmtDate());
                $todo->save();
            } catch (\Exception $e) {
                throw new CouldNotSaveException(__($e->getMessage()));
            }
        }

        return $todo->getData();
    }

    // uses parameter $text
//    public function save($text)
//    {
//
//        try {
//            $todo = $this->objectFactory->create();
    ////            $todo->setData('item_text', $text);
//            $todo->setData('creation_time', $this->_date->gmtDate());
//            $todo->save();
//
//        } catch(\Exception $e) {
//            throw new CouldNotSaveException(__($e->getMessage()));
//        }
//        return $todo->getData();
//    }

    public function getById($id)
    {
        $todo = $this->objectFactory->create();
        $this->objectResourceModel->load($todo, $id);
        if (!$todo->getId()) {
            throw new NoSuchEntityException(__('Object with id ' . $id . ' does not exist.', $id));
        }
        return $todo;
    }

    public function delete(TodoItemInterface $TodoItem)
    {
        try {
            $this->objectResourceModel->delete($TodoItem);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    public function deleteById($id)
    {
        // logger
//        $this->_logger->info('I did deletebyid');
        return $this->delete($this->getById($id));
    }

    public function getList()
    {
        // logger
//        $this->_logger->info('I did getlist');

        // not using searchcriteria
        $collection = $this->collectionFactory->create();
        return $collection->getData();

        // searchcriteria using
//        $searchResults = $this->searchResultsFactory->create();
//        $searchResults->setSearchCriteria($criteria);
//        $collection = $this->collectionFactory->create();
//        // if there's no criteria
//        if (empty($criteria)) return $collection->getData();

        // if there's criteria
//        foreach ($criteria->getFilterGroups() as $filterGroup) {
//            $fields = [];
//            $conditions = [];
//            foreach ($filterGroup->getFilters() as $filter) {
//                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
//                $fields[] = $filter->getField();
//                $conditions[] = [$condition => $filter->getValue()];
//            }
//            if ($fields) {
//                $collection->addFieldToFilter($fields, $conditions);
//            }
//        }
//        $searchResults->setTotalCount($collection->getSize());
//        $sortOrders = $criteria->getSortOrders();
//        if ($sortOrders) {
//            /** @var SortOrder $sortOrder */
//            foreach ($sortOrders as $sortOrder) {
//                $collection->addOrder(
//                    $sortOrder->getField(),
//                    ($sortOrder->getDirection() == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
//                );
//            }
//        }
//        $collection->setCurPage($criteria->getCurrentPage());
//        $collection->setPageSize($criteria->getPageSize());
//        $objects = [];
//        foreach ($collection as $objectModel) {
//            $objects[] = $objectModel;
//        }
//        $searchResults->setItems($objects);
//        return $searchResults;
    }
}
