<?php
namespace Pulsestorm\ToDoCrud\Api;

use Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface TodoItemRepositoryInterface 
{

//    /**
//     * @param int $id
//     * @param string $text
//     * @return mixed
//     */
//    public function put($id, TodoItemInterface $TodoItem);


    /**
     * @param mixed $data
     * @return void
     */
    public function save($data);

    /**
     * @param int $id
     * @return \Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface
     */
    public function getById($id);

    /**
     * @param void
     * @return \Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface
     */
    public function getList();

    /**
     * @param \Pulsestorm\ToDoCrud\Api\Data\TodoItemInterface $TodoItem
     * @return boolean
     */
    public function delete(TodoItemInterface $TodoItem);

    /**
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);
}
