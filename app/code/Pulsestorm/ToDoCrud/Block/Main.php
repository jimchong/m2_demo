<?php
namespace Pulsestorm\ToDoCrud\Block;

use Magento\Framework\Stdlib\DateTime\DateTime;

class Main extends \Magento\Framework\View\Element\Template
{
    protected $col;
    protected $_date;
    protected $todo;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Pulsestorm\ToDoCrud\Model\TodoItemFactory $toDoFactory,
        DateTime $date
    ) {
        $this->_date                = $date;
        $this->toDoFactory = $toDoFactory;
        parent::__construct($context);
    }

    public function _prepareLayout()
    {
        $this->todo = $this->toDoFactory->create();
    }

    public function getCompleteItems()
    {
        return $this->todo->getCollection()->addFieldToFilter('date_completed', ['lteq' => $this->_date->date('Y-m-d')])
            ->addFieldToFilter('date_completed', ['gteq' => $this->_date->date('Y-m-d')]);
        // return $this->todo->addFieldToFilter('date_completed', ['lteq' => $this->_date->date('Y-m-d')])
        //     ->getCollection();
//                         ->addFieldToFilter('date_completed', ['gteq' => $this->_date->date('Y-m-d')]);
    }

    public function getItems()
    {
        return $this->todo->getCollection();
    }
}
