<?php

namespace Jimchong\Hello\Observer\Customer;

use JimChong\Hello\Logger\Logger;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class Authenticated implements ObserverInterface
{
    protected $_logger;

    public function __construct(Logger $logger)
    {
        $this->_logger = $logger;
    }

    public function execute(Observer $observer)
    {
        $customer = $observer->getModel();
        print_r($customer->getData());
        $this->_logger->warn('Customer logged in :' . $customer->getFirstname());
    }
}
