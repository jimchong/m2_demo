<?php

namespace JimChong\Hello\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use JimChong\Hello\Logger\Logger;

class Predispatch implements ObserverInterface
{

    protected $_logger;

    public function __construct(Logger $logger)
    {
        $this->_logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
//        $this->_logger->warn('Observer works jimchong_hello');
    }
}
