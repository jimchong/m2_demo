<?php
namespace Mageplaza\HelloWorld\Observer;

use Magento\Framework\Event\Observer;

class ChangeDisplayText implements \Magento\Framework\Event\ObserverInterface
{
    public function execute(Observer $observer)
    {
        $displayText = $observer->getData('mp_text');
//        echo $displayText->getText() . " - Event <br>";
        $displayText->setText('Catch magento 2 event successfully!!!');
        return $this;
    }
}