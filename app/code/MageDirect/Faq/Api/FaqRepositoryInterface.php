<?php

namespace MageDirect\Faq\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface FaqRepositoryInterface
{
    public function save(Data\FaqInterface $faq);

    public function getById($faqId);

    public function getList(SearchCriteriaInterface $searchCriteria);

    public function delete(Data\FaqInterface $faq);

    public function deleteById($faqId);
}